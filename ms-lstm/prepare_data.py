'''
 Date   : 10 Sep 2018
 Author : Dimitri Kartsaklis

 MS-LSTM data preparation.

 For more details please refer to:

 D. Kartsaklis, M.T. Pilehvar, N. Collier. Mapping Text to Knowledge Graph
    Entities with Multi-Sense LSTMs. In Proceedings of the 2018 Conference
    on Empirical Methods in Natural Language Processing (EMNLP)
    (https://arxiv.org/pdf/1808.07724.pdf)

'''

from keras.preprocessing.text import Tokenizer
import random
import argparse
import sys

def load_data(fname):
    lines = open(fname,'r').readlines()
    data_keys = []
    data_vals = []
    for ln in lines:
        items = ln.strip().split('|')
        if len(items) != 2:
            continue
        cid = items[0]
        txt = items[1]
        data_keys.append(cid)
        data_vals.append(txt)
    return (data_keys,data_vals)

def save_data(fname, data):
    with open(fname,'w') as f:
        for (k,seq) in data:
            seq_str = ' '.join([str(n) for n in seq])
            f.write("%s|%s\n" % (k, seq_str))

if __name__ == "__main__":

    descr = "MS-LSTM data preparation."
    epil  = "See [D. Kartsaklis, M.T. Pilehvar, N. Collier (EMNLP 2018)]"

    parser = argparse.ArgumentParser(description=descr,epilog=epil)
    parser.add_argument('--train_data', required=True,
                         help='File with training data')
    parser.add_argument('--test_data', required=True,
                         help='File with test data')
    parser.add_argument('--dev_data', default="", 
                         help='File with development data')
    args = parser.parse_args()

    # Load data
    train_keys,train_values = load_data(args.train_data)
    test_keys, test_values  = load_data(args.test_data)
    if args.dev_data != "":
        dev_keys,dev_values = load_data(args.dev_data)
    else:
        dev_keys = []
        dev_values = []

    # Tokenize text and convert words to indices
    tok = Tokenizer()
    tok.fit_on_texts(train_values+test_values+dev_values)
    
    train_data = zip(train_keys,tok.texts_to_sequences(train_values))
    test_data  = zip(test_keys, tok.texts_to_sequences(test_values))
    if args.dev_data != "":
        dev_data = zip(dev_keys, tok.texts_to_sequences(dev_values))

    # Save files
    ext = ".seq"
    save_data(args.train_data+ext, train_data)
    save_data(args.test_data+ext, test_data)
    if args.dev_data != "":
        save_data(args.dev_data+ext, dev_data)

    # Save word -> index mapping
    wrd_list = sorted(tok.word_index.items(), key=lambda x:x[1])
    with open('word_mapping.txt','w') as f:
        for (wrd,idx) in wrd_list:
            f.write("%s %d\n" % (wrd,idx))

    print
    print "%d training instances saved at %s" % (len(train_keys),args.train_data+ext)
    print "%d testing instances saved at %s" % (len(test_keys),args.test_data+ext)
    if args.dev_data != "":
        print "%d dev instances saved at %s" % (len(dev_keys),args.dev_data+ext)
    print "Word-index mapping saved in %s" % 'word_mapping.txt'
    print

