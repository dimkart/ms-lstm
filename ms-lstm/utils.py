import numpy as np

def load_vectors(fname):
    lines = open(fname,'r').readlines()

    dct = {}
    for ln in lines:
        ln = ln.strip()
        if ln[0] == '#' or len(ln) == 0:
            continue
        lst = ln.split()
        cid = lst[0]
        vec = np.array([float(x) for x in lst[1:]])
        dct[cid] = vec

    return dct

def load_seq_data(fname):
    lines = open(fname,'r').readlines()
    data = []
    for ln in lines:
        cid,seq = ln.split('|')
        seq_int = [int(n) for n in seq.split()]
        data.append((cid,seq_int))

    return data

