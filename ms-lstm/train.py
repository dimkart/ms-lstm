'''
 Date   : 10 Sep 2018
 Author : Dimitri Kartsaklis

 MS-LSTM training.

 For more details please refer to:

 D. Kartsaklis, M.T. Pilehvar, N. Collier. Mapping Text to Knowledge Graph
    Entities with Multi-Sense LSTMs. In Proceedings of the 2018 Conference
    on Empirical Methods in Natural Language Processing (EMNLP)
    (https://arxiv.org/pdf/1808.07724.pdf)

'''

from keras.models import Model
from keras.callbacks import ModelCheckpoint
from keras.preprocessing.sequence import pad_sequences
from sklearn.preprocessing import normalize
from keras.layers import *
import numpy as np
from ms_embedding import MSEmbedding
import argparse
from utils import *

def prepare_data(data,kvs,maxlen,norm):
    d_array = []
    v_array = []
    for (k,seq) in data:
        if k in kvs:
            d_array.append(seq)
            v_array.append(kvs[k])

    d_array_pad = pad_sequences(d_array, maxlen, padding='post', truncating='post')
    X_arr = np.array(d_array_pad)
    Y_arr = np.array(v_array)
    if norm == '1':
        Y_arr = normalize(Y_arr)

    return (X_arr,Y_arr)

if __name__ == "__main__":

    descr = "Keras implementation for training an MS-LSTM."
    epil  = "See [D. Kartsaklis, M.T. Pilehvar, N. Collier (EMNLP 2018)]"

    parser = argparse.ArgumentParser(description=descr,epilog=epil)
    parser.add_argument('--data', required=True,
                         help='File with training data')
    parser.add_argument('--vspace', required=True,
                         help='Vector space file')
    parser.add_argument('--outfile', required=True,
                         help='File name for saving the trained model')
    parser.add_argument('--dims', type=int, default=150,
                        help='Number of embedding dimensions')
    parser.add_argument('--att_dims', type=int, default=50,
                        help='Number of dimensions in the attentional mechanism')
    parser.add_argument('--senses', type=int, default=3,
                        help='Number of sense embeddings per word')
    parser.add_argument('--maxlen', type=int, default=40,
                        help='Maximum length of an input (in words)')
    parser.add_argument('--vocsize', type=int, required=True,
                        help='Number of words in the vocabulary (train+test data)')
    parser.add_argument('--epochs', type=int, default=20,
                        help='Number of training epochs')
    parser.add_argument('--batchsize', type=int, default=64,
                        help='Size of data batches during training')
    parser.add_argument('--normalise', default='1', choices=['0','1'],
                        help='Normalise entity vectors before training?')
    parser.add_argument('--shuffle', default='1', choices=['0','1'],
                        help='Shuffle training data at each epoch?')
    parser.add_argument('--bilstm', default='0', choices=['0','1'],
                        help='Use a bi-LSTM instead of an LSTM?')
    args = parser.parse_args()

    print "Loading data..."
    data = load_seq_data(args.data)
    kvs  = load_vectors(args.vspace)

    # Prepare input for the model
    X,Y = prepare_data(data,kvs,args.maxlen,args.normalise)
    out_dims = Y.shape[1]

    # Create NN
    inp = Input(shape=(args.maxlen,))
    e1 = Embedding(args.vocsize+1, args.dims, input_length=args.maxlen, 
                   mask_zero=True)(inp)
    m1 = MSEmbedding(args.vocsize+1,senses=args.senses,mask_zero=True,
                     att_dims=args.att_dims)([e1,inp])
    d1 = Dropout(0.20)(m1)

    if args.bilstm == '1':
        l1 = Bidirectional(LSTM(200, return_sequences=True))(d1)
    else:
        l1 = LSTM(200, return_sequences=True)(d1)

    d2 = Dropout(0.20)(l1)

    if args.bilstm == '1':
        l2 = Bidirectional(LSTM(out_dims, return_sequences=False))(d2)
        y1 = Dense(out_dims)(l2)
    else:
        y1 = LSTM(out_dims, return_sequences=False)(d2)

    model = Model(inputs=inp,outputs=y1)
    model.compile(loss='mse',optimizer='adam')
    chkpt = ModelCheckpoint(args.outfile,monitor='loss',period=1,save_best_only=True)

    # Train model
    print "Training..."
    model.fit(X, Y,
              epochs=args.epochs,
              batch_size=args.batchsize,
              callbacks=[chkpt],
              verbose=1,
              shuffle=(args.shuffle=='1')
    )


