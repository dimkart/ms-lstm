# -*- coding: utf-8 -*-

'''
 Date   : 10 Sep 2018
 Author : Dimitri Kartsaklis

 Extend a graph with textual features from a list of texts.

 For more details please refer to:

 D. Kartsaklis, M.T. Pilehvar, N. Collier. Mapping Text to Knowledge Graph
    Entities with Multi-Sense LSTMs. In Proceedings of the 2018 Conference
    on Empirical Methods in Natural Language Processing (EMNLP)
    (https://arxiv.org/pdf/1808.07724.pdf)

'''

import sys
import unicodedata
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
import argparse

    
if __name__ == "__main__":

    descr = "Extends a graph with textual features from a list of texts."
    epil  = "See [D. Kartsaklis, M.T. Pilehvar, N. Collier (EMNLP 2018)]"

    parser = argparse.ArgumentParser(description=descr,epilog=epil)
    parser.add_argument('--txt_data', required=True,
                         help='File with text data assigned to entities')
    parser.add_argument('--edgelist', required=True,
                         help='File with edge list of existing graph')
    parser.add_argument('--outfile', required=True,
                         help='File for storing the extended edge list')
    parser.add_argument('--threshold', type=float, default=0.0,
                        help='TF-IDF threshold for including textual features (0=include all)')

    args = parser.parse_args()

    print "Loading..."
    with open(args.txt_data,'r') as f:
        txt_lines = f.readlines()
    
    txt_data = {}
    for ln in txt_lines:
        arg_list = ln.strip().split('|')
        if len(arg_list) != 2:
            continue
        cid,text = arg_list

        # Concatenate all the data for the same
        # entity in a single paragraph
        if cid not in txt_data:
            txt_data[cid] = text
        else:
            if txt_data[cid][-1] != '.':
                txt_data[cid] = txt_data[cid] + '. ' + text
            else:
                txt_data[cid] = txt_data[cid] + ' ' + text

    doc_keys = txt_data.keys()
    doc_list = txt_data.values()

    print "Computing TF-IDF values..."
    vect = TfidfVectorizer(stop_words='english',encoding='utf-8',decode_error='ignore')
    M = vect.fit_transform(doc_list)
    feat_names = vect.get_feature_names()

    # Save a mapping (entity:list of textual features) in a file
    data_new = {}
    out = open(args.outfile+'.tfidf','w')

    for cidx in range(M.shape[0]):

        # Get only the relevant text features
        _,row = np.nonzero(M[cidx,:])
        widx_list = row.flatten().tolist()

        cid = doc_keys[cidx]
        data_new[cid] = []

        for widx in widx_list:
            wrd = feat_names[widx]
            n = round(M[cidx,widx],2)
            if n >= args.threshold:
                data_new[cid].append((wrd,n))

        if len(data_new[cid]) > 0:
            srt_list = sorted(data_new[cid],key = lambda x:x[1],reverse=True)
            out_str = str(cid)+"|"+"|".join(["%s %.2f" % t for t in srt_list]) 
            out.write(out_str+"\n")

    out.close()
    
    # Save merged edge list
    with open(args.edgelist,'r') as f:
        edge_lines = f.readlines()

    with open(args.outfile,'w') as f:

        # First save the normal edge list
        for ln in edge_lines:
            arg_list = ln.strip().split()
            if len(arg_list) == 2:   # No weight provided, add 1.0
                f.write("%s %s 1.0\n" % tuple(arg_list))
            elif len(arg_list) == 3: # Weight is provided
                f.write(ln)
            else:
                continue

        # Then append the textual feature edge list
        for conid in data_new:
            for (wrd,w) in data_new[conid]:
                f.write("%s %s %.2f\n" % (conid,'TF:'+wrd,w))

