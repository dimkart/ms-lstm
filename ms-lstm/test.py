'''
 Date   : 10 Sep 2018
 Author : Dimitri Kartsaklis

 MS-LSTM testing.

 For more details please refer to:

 D. Kartsaklis, M.T. Pilehvar, N. Collier. Mapping Text to Knowledge Graph
    Entities with Multi-Sense LSTMs. In Proceedings of the 2018 Conference
    on Empirical Methods in Natural Language Processing (EMNLP)
    (https://arxiv.org/pdf/1808.07724.pdf)

'''

from __future__ import division
import numpy as np
from scipy.spatial.distance import cosine
from keras.models import load_model
from keras.layers import *
from keras.preprocessing.sequence import pad_sequences
import keras.backend as K
import sys
from ms_embedding import MSEmbedding
from sklearn.preprocessing import normalize
from utils import *
import argparse

def mrr(conid,ranked_list):
    id_lst = zip(*ranked_list)[0]
    return 1 / (id_lst.index(conid)+1)

def acc(conid,ranked_list):
    return conid == ranked_list[0][0]

def top_acc(conid, ranked_list, thr):
    return conid in zip(*ranked_list)[0][:thr]

def prepare_data(data,kvs):
    test_keys = []
    test_vals = []
    vec_lst = []
    for (k,seq) in data:
        if k in kvs:
            test_keys.append(k)
            test_vals.append(seq)
            vec_lst.append(kvs[k])

    arr = normalize(np.array(vec_lst))
    return (test_keys,test_vals,arr)

if __name__ == "__main__":

    descr = "Keras implementation for testing an MS-LSTM."
    epil  = "See [D. Kartsaklis, M.T. Pilehvar, N. Collier (EMNLP 2018)]"

    parser = argparse.ArgumentParser(description=descr,epilog=epil)
    parser.add_argument('--data', required=True,
                         help='File with testing data')
    parser.add_argument('--vspace', required=True,
                         help='Vector space file (same as in training)')
    parser.add_argument('--model', required=True,
                         help='File for trained Keras model')
    parser.add_argument('--maxlen', type=int, default=40,
                        help='Maximum length of an input (in words) -- same as in training')
    args = parser.parse_args()

    print "Loading data..."
    data = load_seq_data(args.data)
    kvs  = load_vectors(args.vspace)

    test_keys,test_vals,Y = prepare_data(data,kvs)

    print "Reading model..."
    model = load_model(args.model,custom_objects={'MSEmbedding': MSEmbedding})

    print "Evaluating..."
    mrr_list = []
    acc_list = []
    topacc_list = []

    entry_idx = 0
    for (conid,tseq) in zip(test_keys,test_vals):
        seq = pad_sequences([tseq], args.maxlen, padding='post', truncating='post')
        y_pred = model.predict(seq, batch_size=1)
        y_pred = y_pred.flatten()

        y_pred = y_pred / np.linalg.norm(y_pred)

        res = np.dot(Y,y_pred).flatten().tolist()
        lst = zip(res,range(len(res)))
        lst = sorted(lst,key=lambda x:x[0],reverse=True)
        
        res = [(test_keys[idx],dst) for (dst,idx) in lst] 

        # Compute and store various scores
        mrr_list.append(mrr(conid, res))
        acc_list.append(acc(conid, res))
        topacc_list.append(top_acc(conid, res, 20))

        entry_idx += 1
        mrr_tmp = np.sum(mrr_list) / entry_idx
        acc_tmp = np.sum(acc_list) / entry_idx
        topacc_tmp = np.sum(topacc_list) / entry_idx
        sys.stdout.write("Computing scores (%d of %d): MRR=%.2f, Acc=%.2f, Acc-20=%.2f  \r" % \
                (entry_idx,len(test_keys),mrr_tmp,acc_tmp,topacc_tmp))
        sys.stdout.flush()

    print "\n"

