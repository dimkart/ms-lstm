'''
 Date   : 10 Sep 2018
 Author : Dimitri Kartsaklis

 MSEmbedding layer implementing dynamic disambiguation of embeddings
 during training.

 For more details please refer to:

 D. Kartsaklis, M.T. Pilehvar, N. Collier. Mapping Text to Knowledge Graph
    Entities with Multi-Sense LSTMs. In Proceedings of the 2018 Conference
    on Empirical Methods in Natural Language Processing (EMNLP)
    (https://arxiv.org/pdf/1808.07724.pdf)

'''

from keras.layers import Layer
import keras.backend as K
from keras.activations import relu,softmax

if K.backend() == 'theano':
    import theano.tensor
else:
    import tensorflow as tf

class MSEmbedding(Layer):
    """Multi-Sense embedding layer. 

    Must always follow right after a normal Embedding layer.

    # Arguments
        input_dim: int > 0. Size of the vocabulary,
	        i.e. maximum integer index + 1.
	    senses: int > 1. Number of sense embeddings.
	    mask_zero: Whether or not the input value 0 is a special 
	        "padding" value that should be masked out.
	    att_dims: int >= 1. Dimensions of the attention layer.

    # Input shape
        List of two tensors:
            * 3D tensor with shape: `(batch_size,sequence_length,output_dim)`
	            This is the output of the preceding Embedding layer.
	        * 2D tensor with shape: `(batch_size,sequence_length)`
	            This is the input of the preceding Embedding layer (also
		        required here so the layer knows which embeddings to
		        update).

    # Output shape
        3D tensor with shape: `(batch_size,sequence_length,output_dim)`

    # References
        - [Mapping Text to Knowledge Graph Entities using Multi-Sense
	       LSTMs](https://arxiv.org/pdf/1808.07724.pdf)
    """

    def __init__(self, input_dim, senses=3, mask_zero=True, att_dims=50, **kwargs):
        self.mask_zero = mask_zero        # Masking support
        self.input_dim = input_dim        # Size of vocabulary
        self.senses = senses              # Number of senses
        self.att_dims = att_dims          # Dimensions of the first attention layer
        super(MSEmbedding,self).__init__(**kwargs)
 

    def build(self, input_shape):
        emb_shape, seq_shape = input_shape 
        self.ms_embeddings = self.add_weight(shape=(self.input_dim, self.senses, emb_shape[2]),
                                     initializer='uniform',
                                     name='ms_embeddings',
                                     trainable=True)
        self.att1 = self.add_weight(shape=(emb_shape[2] * 2, self.att_dims),
                                     initializer='uniform',
                                     name='att_1',
                                     trainable=True)
        self.att1_bias = self.add_weight(shape=(self.att_dims,),
                                     initializer='uniform',
                                     name='att_1_bias',
                                     trainable=True)
        self.att2 = self.add_weight(shape=(self.senses * self.att_dims, self.senses),
                                     initializer='uniform',
                                     name='att_2',
                                     trainable=True)
        self.att2_bias = self.add_weight(shape=(self.senses,),
                                     initializer='uniform',
                                     name='att_2_bias',
                                     trainable=True)
        super(MSEmbedding,self).build(input_shape)
    

    def compute_mask(self, inputs, mask=None):
        emb, seq = inputs
        if not self.mask_zero:
            return None
        else:
            return K.not_equal(seq, 0)

    def call(self, inputs):

        # Expecting a list as input
        # 1st item = embeddings, 2nd item = word index sequence
        #    emb = (B,h,d)     seq = (B,h)
        emb,seqs = inputs

        # Get shapes
        B,h,d = K.int_shape(emb)
        k = self.senses
        l = self.att_dims

        # Prepare contexts
        tmp = K.sum(emb,axis=1)
        cnt = tmp[:,None,:] - emb    # (B,h,d)

        # Gather embeddings in a tensor
        if K.dtype(seqs) != 'int32':
            seqs = K.cast(seqs, 'int32')
        out = K.gather(self.ms_embeddings,seqs) # (B,h,k,d)

        cnt_exp1 = K.tile(cnt,(1,1,k)) 
        cnt_exp2 = K.reshape(cnt_exp1,(K.shape(cnt_exp1)[0],h,k,d))
        cnt_out = K.concatenate([out,cnt_exp2],axis=-1)   # (B,h,k,2d)

        # Forward pass on first attention layer
        att_out1 = K.dot(cnt_out,self.att1)     # (B,h,k,l)
        att_out1 = K.bias_add(att_out1, self.att1_bias)
        att_out1 = relu(att_out1)

        # Forward pass on second attention layer (softmax)
        att_out2 = K.reshape(att_out1,(K.shape(att_out1)[0],h,k*l))  # (B,h,kl)
        att_out2 = K.dot(att_out2,self.att2)      # (B,h,k)
        att_out2 = K.bias_add(att_out2, self.att2_bias)
        att_out2 = softmax(att_out2)

        dt = att_out2

        # Weight the context by the similarity with the senses
        wcnt = dt[:,:,:,None] * cnt[:,:,None,:]

        # Update the sense embedding layer with context
        if K.backend() == 'theano':
            theano.tensor.inc_subtensor(self.ms_embeddings[seqs],wcnt)
        else:
            tf.scatter_add(self.ms_embeddings,seqs,wcnt)

        # Compute the output of the layer
        out  = out + wcnt 
        res = K.mean(out,axis=2)
        
        return res

    def compute_output_shape(self,input_shape):
        emb_shp,seqs_shp = input_shape
        return emb_shp

    def get_output_shape_for(self, input_shape):
        emb_shp,seqs_shp = input_shape
        return emb_shp

    def get_config(self):
        config = {
                   'input_dim' : self.input_dim,
                   'senses'    : self.senses,
                   'mask_zero' : self.mask_zero,
                   'att_dims'  : self.att_dims
                 }
        base_config = super(MSEmbedding, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

