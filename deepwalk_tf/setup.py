#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

requirements = [
    'wheel>=0.23.0',
    'Cython>=0.20.2',
    'argparse>=1.2.1',
    'futures>=2.1.6',
    'six>=1.7.3',
    'gensim>=1.0.0',
    'scipy>=0.15.0',
    'psutil>=2.1.1'
]

setup(
    name='deepwalk_tf',
    version='1.0.0',
    description='DeepWalk supporting biased random walks on graphs extended with textual features',
    author='B. Perozzi/D. Kartsaklis',
    author_email='dimkart@gmail.com',
    packages=[
        'deepwalk_tf',
    ],
    install_requires=requirements,
    license="GPLv3",
    zip_safe=False
)
